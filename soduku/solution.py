# -*- coding: utf-8 -*-

from soduku.colors import *

# numberType: 0 for blank, -1 for question, 1 for trial.
class sodukuGrid:
	def __init__(self, number, numberType):
		'''number: Number in the grid (1-9). numberType: The number in this grid is given by the question(-1), is guessed by program(1), or is blank(0).'''
		self.number = number
		self.numberType = numberType
	
	def showNumber(self):
		if self.numberType == -1:
			return ansiYellow + '%d' % self.number + ansiReset
		elif self.numberType == 0:
			return r' '
		else:
			return ansiGreen + '%d' % self.number + ansiReset

def showRow(sodukuRow):
	ls = []
	for i in sodukuRow:
		ls.append(i.showNumber())
	ls.insert(9, '|')
	ls.insert(6, '|')
	ls.insert(3, '|')
	s = ''
	for i in ls:
		s += i
	return s

def showWhole(sodukuWhole):
	ls = []
	for i in sodukuWhole:
		ls.append(showRow(i))
	ls.insert(9, '------------')
	ls.insert(6, '------------')
	ls.insert(3, '------------')
	for i in ls:
		print(i)


def checkStatus(sodukuWhole, i, j):
	row = sodukuWhole[i]
	col = sodukuWhole[:, j]
	ii = i - i % 3
	jj = j - j % 3
	grid = []
	for iii in [ii, ii + 1, ii + 2]:
		for jjj in [jj, jj + 1, jj + 2]:
			grid.append(sodukuWhole[iii][jjj])

	boo = True
	for st in [row, col, grid]:
		cleanList = []
		for item in st:
			if item.number != 0:
				if item.number in cleanList:
					return False
				else:
					cleanList.append(item.number)
	return boo

def checkOneToNine(sodukuSet):
	standardSet = set([i for i in range(1, 10)])
	return sodukuSet == standardSet

def checkWhole(sodukuWhole):
	booRowList = []
	booColList = []
	booGridList = []

	for i in range(0, 9):
		rowSet = set([j.number for j in sodukuWhole[i]])
		colSet = set([j.number for j in sodukuWhole[:, i]])
		booRowList.append(checkOneToNine(rowSet))
		booColList.append(checkOneToNine(colSet))

	if False in booRowList or False in booColList:
		return False

	flattenedGrids = [sodukuWhole[0:3, 0:3].flatten(), sodukuWhole[0:3, 3:6].flatten(), sodukuWhole[0:3, 6:9].flatten(),
	sodukuWhole[3:6, 0:3].flatten(), sodukuWhole[3:6, 3:6].flatten(), sodukuWhole[3:6, 6:9].flatten(),
	sodukuWhole[6:9, 0:3].flatten(), sodukuWhole[6:9, 3:6].flatten(), sodukuWhole[6:9, 6:9].flatten()]
	
	for i in flattenedGrids:
		gridSet =  set([j.number for j in i])
		booGridList.append(checkOneToNine(gridSet))

	if False in booGridList:
		return False
	else:
		return True
