# -*- coding: utf-8 -*-

ansiReset = "\x1b[0m"
ansiRed = "\x1b[31m"
ansiGreen = "\x1b[32m"
ansiYellow = "\x1b[33m"
ansiBlue = "\x1b[34m"
