# -*- coding: utf-8 -*-

import sys
import numpy as np
from soduku.solution import *

if __name__ == '__main__':
	sodukuWhole = []
	for i in range(0, 9):
		sodukuRow = [sodukuGrid(0, -1) for i in range(0, 9)]
		sodukuWhole.append(sodukuRow)
	sodukuWhole = np.array(sodukuWhole)

	fileName = sys.argv[1]
	for line, row in zip(open(fileName), sodukuWhole):
		line = line.strip()
		for l, r in zip(line, row):
			if l == '*':
				r.number = 0
				r.numberType = 0
			else:
				r.number = int(l)
				r.numberType = -1

	print("The question is\n")
	showWhole(sodukuWhole)

	solutionStack = []
	pos = 0

	while checkWhole(sodukuWhole) == False:
		i = pos / 9
		j = pos % 9
		ij = sodukuWhole[i][j]

		if ij.numberType == -1:
			pos += 1
		else:
			ij.number += 1
			if ij.number == 10:
				ij.number = 0
				ij.numberType = 0
				pos = solutionStack.pop()
			else:
				if checkStatus(sodukuWhole, i, j) == True:
					ij.numberType = 1
					solutionStack.append(pos)
					pos += 1

	print("\nThe solution is\n")
	showWhole(sodukuWhole)
